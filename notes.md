# How I got into open source software

* First pull request: https://github.com/ywen/pivotal_to_pdf/pull/1
  * Empowering
  * I can fix it!
  * I can help others!
  
* From there:
  * Ruby meetup
  * Some small contributions to
    * hacketyhack https://github.com/hacketyhack/hacketyhack/pull/93
    * shoes https://github.com/shoes/shoes/pull/81
  * Confidence to read code of Ruby gems I was using
  
* Rstat.us
  * The dream of sticking it to the man!
  * The dream dying!
  
* Today:
  * On the core team for the Rust programming language
  * Co-author of The Rust Programming Language book
  * Have gotten to collaborate with awesome people all over the world
  * Rustlings - # of stars = people helped, warm fuzzies
  
* Not all butterflies and rainbows
  * Tragedy of the commons
  * Entitled people who only complain when they could be helping
  * Corporations profiting off your volunteer work
